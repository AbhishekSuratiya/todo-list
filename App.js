import React, { useState } from 'react';
import './App.css'; 

function App(){
  const[task,setTask]=useState('');
  const[taskArray,setTaskArray]=useState([]);
  
  function event(e){
    setTask(e.target.value);
  }

  function add(){
    setTaskArray((prev)=>{
       return([...prev,task]) 
    });
    setTask('');
  }


  function TaskComponent(props){
    return(
      <div className='crossAndTask'>
        <div onClick={()=>
          {
            props.delete(props.id)
          }} className='cross'>X</div>
        <div className="task">
          {props.task}
        </div>
      </div>
    )
  }
  function deleteFun(id){
    console.log('deleted');
    setTaskArray((prev)=>{
      return (
        prev.filter((element,index)=>{
          return index!==id;
        })
      )
    })
  }

  return(
    <>
      <div className="container">
        <div className="todoContainer">
          <div className="todoHeading">ToDo List</div>
          <div className="inputAndButton">
            <input onChange={event} className="addTaskInput" type='text' value={task} placeholder="Add task"></input>
            <button onClick={task&&task?add:console.log()} className='addButton'>Add</button> 
          </div>  
          <div className="taskContainer">
              {
                taskArray.map((val,index)=>{
                  return (
                    <TaskComponent key={index} id={index} task={val} delete={deleteFun}/>
                    )
                })
              }
            </div>
        </div>
      </div>
    </>
  )
}

export default App;